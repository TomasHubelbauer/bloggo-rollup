# Migrate to mainline Rollup after the sourcemap fix release

[The sourcemap fix](https://github.com/rollup/rollup/pull/2012)

Just `yarn add rollup` followed by changing the `start` command to just `rollup -c`.
