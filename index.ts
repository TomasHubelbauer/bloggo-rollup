import * as jsqr from 'jsqr';
import * as qrcode_ from 'qrcode-generator';

const qrcode: typeof qrcode_ = qrcode_;

const qr: QRCode = qrcode(0, 'H');
qr.addData('Rollup');

window.addEventListener('load', _ => {
  document.body.innerHTML = qr.createImgTag(10);
});
