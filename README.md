# Rollup

## Installing

`yarn global add rollup`

I am using a fork with a fix that was preventing me from seeing TypeScript erros due to a sourcemap consumption problem.

- `yarn add adrianheine/rollup#sourcemap`
- `cd node_modules/rollup`
- `yarn`
- `cd ../..`
- `yarn start`

The above allowed me to obtain and build the fork and in addition to it, I've changed the invocation of `rollup` to:

`node node_modules/rollup/bin/rollup -c`

Now my TypeScript errors flow right through.

## Bundling

`rollup index.js -o bundle.js -f iife`

Or use a `rollup.config.js` file, see [Configuring](#configuring).

## Configuring

`rollup.config.js`:

```javascript
export default {
  input: 'index.js',
  output: {
    file: 'bundle.js',
    format: 'iife'
  }
};
```

`rollup -c`

## Integrating

**`rollup-plugin-node-resolve`**

For importing Node packages.

**`rollup-plugin-commonjs`**

For Node packages which do not expose ES6 modules but use CJS.

**`rollup-plugin-typescript2`**

The *real* TypeScript plugin, `rollup-plugin-typescript` does not support TS2.

## Troubleshooting

**Cannot call a namespace**:

Happens when using Rollup with code like this:

```typescript
import * as qrcode from 'qrcode-generator';
```

The fix is this:

```typescript
import * as qrcode_ from 'qrcode-generator';
const qrcode: typeof qrcode_ = qrcode_;
// WTF I know…
```

**“version” is a required argument**:

[There is now a PR with a fix, see the related todo](https://github.com/rollup/rollup/pull/2012)

This only obscures the real underlying issue, which is often *Cannot call a namespace* as seen above.
